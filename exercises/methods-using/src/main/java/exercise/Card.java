package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String str = cardNumber.substring(cardNumber.length() - 4);
        System.out.println("*".repeat(starsCount) + str);
        // END
    }
}
