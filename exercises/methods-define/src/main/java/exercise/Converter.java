package exercise;

class Converter {
    // BEGIN
    public static int convert(int number, String typeConversion) {
        switch (typeConversion) {
            case ("Kb"):
                return number / 1024;
            case ("b"):
                return number * 1024;
            case ("mb"):
                return number / 1024;
            default:
                return 0;
        }
    }

    public static void main(Object o) {
        int num = 10;
        String typeC = "b";
        int value = Converter.convert(10, "b");

        System.out.println(num + " Kb" + " = " + value + " " + typeC);
    }

    // END
}

