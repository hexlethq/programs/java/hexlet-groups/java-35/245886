package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int a, int b, int sqSin) {
        return (a * b * Math.sin(sqSin * Math.PI / 180)) / 2;
    }

    public static void main(String[] args) {
//        System.out.printf("%.1f", Triangle.getSquare(10, 10, 60));
        System.out.printf("%.1f", Triangle.getSquare(4, 5, 45));
    }

    // END
}

