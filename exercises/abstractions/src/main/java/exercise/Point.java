package exercise;

class Point {
    // BEGIN


    public static void main(String[] args) {
        int[] point = Point.makePoint(3, 4);
        Point.getX(point); // 3
        Point.getY(point); // 4
        Point.pointToString(point); // "(3, 4)"

        Point.getQuadrant(point); // 1
        int[] point2 = makePoint(3, -3);
        Point.getQuadrant(point2); // 4
        int[] point3 = makePoint(0, 7);
        Point.getQuadrant(point3); // 0

        int[] point4 = makePoint(-3, -6);
        int[] symmetricalPoint = getSymmetricalPointByX(point4);
        Point.pointToString(symmetricalPoint); // (-3, 6)

        int[] point5_1 = makePoint(0, 0);
        int[] point5_2 = makePoint(3, 4);
        int pointDistance = Point.calculateDistance(point5_1, point5_2); // 5
        Point.printToDistance(pointDistance); // 5
    }

    public static int getQuadrant(int[] point) {
        if (point[0] > 0 && point[1] > 0) {
            return 1;
        }
        if (point[0] < 0 && point[1] > 0) {
            return 2;
        }
        if (point[0] < 0 && point[1] < 0) {
            return 3;
        }
        if (point[0] > 0 && point[1] < 0) {
            return 4;
        }
        return 0;
    }

    public static String pointToString(int[] point) {
        System.out.println("(" + point[0] + ", " + point[1] + ")");
        return "(" + point[0] + ", " + point[1] + ")";
    }

    public static String printToDistance(int distance) {
        System.out.println("" + distance + "");
        return "" + distance;
    }

    public static int getY(int[] point) {
        return point[1];
    }

    public static int getX(int[] point) {
        return point[0];
    }

    public static int[] makePoint(int i, int i1) {
        return new int[]{i, i1};
    }

    public static int[] getSymmetricalPointByX(int[] point) {
        return new int[]{point[0], -point[1]};
    }

    public static int calculateDistance(int[] pointX, int[] pointY) {
//        var a = Math.hypot(pointY[0] - pointX[0]);
// Решение по возврату координат новой точки point
//        int p1 = (int) Math.sqrt(Math.pow((pointY[0] - pointX[0]), 2));
//        int p2 = (int) Math.sqrt(Math.pow((pointY[1] - pointX[1]), 2));
//        return new int[] {p1, p2};

        return (int) Math.sqrt(Math.pow((pointY[0] - pointX[0]), 2) + Math.pow((pointY[1] - pointX[1]), 2));
    }
    // END
}
