package exercise;

class App {
    public static void numbers() {
        // BEGIN
        System.out.println((8 / 2) + (100 % 3));
        // END
    }

    public static void strings() {
        String language = "Java";
        // BEGIN
        var firstText = "Java";
        var secondText = "JVM";
        System.out.println(firstText + " works" + " " + "on " + secondText);
        // END
    }

    public static void converting() {
        Number soldiersCount = 300;
        String name = "spartans";
        // BEGIN
        System.out.println(soldiersCount + " " + name);
        // END
    }

    public static void main(String[] args) {
        numbers();
        strings();
        converting();
    }
}
