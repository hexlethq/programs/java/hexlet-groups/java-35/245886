package exercise;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

class App {
    // BEGIN

    public static String buildList(String[] arrOfAnimals) {
        StringBuilder htmlPrint = new StringBuilder();

        if (arrOfAnimals.length != 0) {
            htmlPrint.append("<ul>\n");

            for (var element : arrOfAnimals) {
                var value = "  " + "<li>" + element + "</li>\n";
                htmlPrint.append(value);
            }

            htmlPrint.append("</ul>");
        }
        return htmlPrint.toString();
    }
    // END

    public static String getUsersByYear(String[][] users, int i) {
        StringBuilder htmlPrint = new StringBuilder();
        StringBuilder value = new StringBuilder();

        if (users.length != 0) {
            for (var user : users) {
                if (Integer.parseInt(user[1].substring(0, 4)) == i) {
                    value.append("  " + "<li>" + user[0] + "</li>\n");
                }
            }

            if (!value.isEmpty()) {
                htmlPrint.append("<ul>\n");
                htmlPrint.append(value);
                htmlPrint.append("</ul>");
            }
        }
        return htmlPrint.toString();
    }

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN

        Date dateFromString = new Date(date);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

        String johnSmith = "";
        try {
            for (var user : users) {
                if (formatter.parse(user[1]).getDate() >= dateFromString.getDate() &&
                        formatter.parse(user[1]).getYear() == dateFromString.getYear()) {
                    johnSmith = user[0];
//                    System.out.println(user[0]);
                }
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }

        return johnSmith;
        // END
    }

    public static void print(String str) {
        System.out.println(str);
    }

    public static void main(String[] args) throws Exception {
        String[] animals = {"cats", "dogs", "birds"};
        print(buildList(animals));

        String[][] users = {
                {"Andrey Petrov", "1990-11-23"},
                {"Aleksey Ivanov", "1995-02-15"},
                {"John Smith", "1990-03-11"},
                {"Vanessa Vulf", "1985-11-16"},
                {"Vladimir Nikolaev", "1990-12-27"},
                {"Alice Lucas", "1986-01-01"},
        };

        System.out.println(getUsersByYear(users, 1990));


        String[][] youngUsers = {
                {"Andrey Petrov", "1990-11-23"},
                {"Aleksey Ivanov", "2000-02-15"},
                {"Anna Sidorova", "1996-09-09"},
                {"John Smith", "1989-03-11"},
                {"Vanessa Vulf", "1985-11-16"},
                {"Vladimir Nikolaev", "1990-12-27"},
                {"Alice Lucas", "1986-01-01"},
                {"Elsa Oscar", "1989-03-10"},
        };
        App.getYoungestUser(youngUsers, "11 Jul 1989"); // "John Smith"
    }
}
