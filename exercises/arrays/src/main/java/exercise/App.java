package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int[] reverse(int[] arrayReverse) {
        int[] reversed = new int[arrayReverse.length];
        int inc = 0;

        if (arrayReverse.length != 0) {
            for (var i = arrayReverse.length - 1; i >= 0; i--) {
                reversed[inc++] = arrayReverse[i];
            }
            System.out.println(Arrays.toString(reversed));
            return reversed;
        } else return arrayReverse;
    }

    public static int mult(int[] arrayMult) {
        int resultMult = 1;

        for (var i = 0; i < arrayMult.length; i++) {
            resultMult *= arrayMult[i];
        }
        return resultMult;
    }

    public static int[] flattenMatrix(int[][] matrix) {
        int matrixLength = 0;

        // Вычисление длинны двухмерного массива
        for (var lvl = 0; lvl < matrix.length; lvl++) {
            matrixLength += matrix[lvl].length;
        }
//        System.out.println("-> " + matrixLength);

        // Длинна одномерного массивы получается из общего количества элементов двухмерного массива
        int[] result = new int[matrixLength];
        int inc = 0;

        if (matrixLength != 0) {
            System.out.println("Good");

            // Добавление всех элементов матрицы в одномерный массив
            for (var i = 0; i < matrix.length; i++) {
                for (var j = 0; j < matrix.length; j++) {
                    result[inc++] = matrix[i][j];
                }
            }
            System.out.println(Arrays.toString(result));

            return result;

        } else {
            System.out.println("Empty");
            return result;
        }
    }

    public static void main(String[] args) {
        int[] numbers = {1, 4, 3, 4, 5};
//        int[] numbers = {};
        reverse(numbers);
        mult(numbers);

        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
        };

        flattenMatrix(matrix);

        int[][] matrix1 = new int[0][0];
        flattenMatrix(matrix1);
    }
    // END
}
