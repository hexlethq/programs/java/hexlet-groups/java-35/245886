package exercise;

import java.util.Arrays;

class App {
    // BEGIN

    public static void main(String[] args) {
        int[] numbers = {3, 10, -1, 4, 3, 42, -5};
        sort(numbers);

        int[] numbers2 = {3, 10, -1, 4, 3, 42, -5};
        selectionSort(numbers2);
    }

    public static int[] sort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = i; j > 0; j--) {
                if (arr[j] < arr[j - 1]) {
                    arr[j] += arr[j - 1];
                    arr[j - 1] = arr[j] - arr[j - 1];
                    arr[j] = arr[j] - arr[j - 1];
                }
            }
        }
        System.out.println(Arrays.toString(arr));
        return arr;
    }

    private static void selectionSort(int[] arr) {

        for (int i = 0; i < arr.length; i++) {
            int min = arr[i];
            int position = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < min) {
                    min = arr[j];
                    position = j;
                }
            }
            arr[position] = arr[i];
            arr[i] = min;
        }
        System.out.println(Arrays.toString(arr));
    }
    // END
}
