package exercise;

import java.util.ArrayList;
import java.util.Arrays;

class App {
    // BEGIN

    public static void print(String str) {
        System.out.print(str);
    }

    public static String getAbbreviation(String str) {
//        String[] splitWords = str.trim().replace("[\s]{2, }", "**").toString();
        String[] splitWords = str.split(" ");
        String abbr = "";

         for (var word: splitWords) {
             if (!word.isEmpty()) {
                 abbr += Character.toUpperCase(word.charAt(0));
             }
         }
        return abbr;
    }

    public static void main(String[] args) {
        getAbbreviation("Complementary metal oxide semiconductor");
    }
    // END
}
