package exercise;

// BEGIN
public class Segment {
    Point point1;
    Point point2;

    public Segment(Point _point1, Point _point2) {
        this.point1 = _point1;
        this.point2 = _point2;
    }

    public Point getBeginPoint() {
        return point1;
    }

    public Point getEndPoint() {
        return point2;
    }

    public Point getMidPoint() {
        return new Point(((point1.x + point2.x) / 2), ((point1.y + point2.y) / 2));
    }
}
// END
