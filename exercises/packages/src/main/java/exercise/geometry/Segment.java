package exercise.geometry;

import java.awt.*;
import java.util.Arrays;

// BEGIN
public class Segment {
    public static double[][] makeSegment(double[] point1, double[] point2) {
        return new double[][] {{point1[0], point1[1]}, {point2[0], point2[1]}};
    }

    public static double[] getBeginPoint(double[][] segment) {
        return new double[] {segment[0][0], segment[0][1]};
    }

    public static double[] getEndPoint(double[][] segment) {
        return new double[] {segment[1][0], (int) segment[1][1]};
    }
}
// END
