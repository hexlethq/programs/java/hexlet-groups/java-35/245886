package exercise;

// BEGIN

import java.util.Arrays;

public class Kennel {
    static String[][] puppy;

    static void addPuppy(String[] _puppy) {
        if (puppy == null) {
            puppy = new String[][]{_puppy};
        } else {
            String[][] morePuppy = new String[puppy.length + 1][];
            int inc = 0;

            for (int i = 0; i < puppy.length; i++) {
                morePuppy[i] = puppy[i];
                inc++;
            }
            morePuppy[inc++] = _puppy;

            puppy = Arrays.copyOf(morePuppy, morePuppy.length);
        }
    }

    public static void addSomePuppies(String[][] puppies2) {
        for (int i = 0; i < puppies2.length; i++) {
            addPuppy(puppies2[i]);
        }
    }

    public static boolean isContainPuppy(String buddy) {
        boolean isContain = false;

        for (String[] strings : puppy) {
            if (strings[0].equals(buddy)) {
                isContain = true;
                break;
            }
        }
//        System.out.println(isContain);
        return isContain;
    }

    public static String[][] getAllPuppies() {
        return puppy;
    }

    public static String[] getNamesByBreed(String chihuahua) {
        String[] breed = new String[0];
        String findBreed;

        for (int i = 0; i < puppy.length; i++) {
            if (puppy[i][1].equals(chihuahua)) {
                findBreed = puppy[i][0];
                breed = Arrays.copyOf(breed, breed.length + 1);
                breed[breed.length - 1] = findBreed;
            }
        }

        return breed;
    }

    public static void resetKennel() {
        puppy = new String[][]{};
    }

    public static int getPuppyCount() {
//        System.out.println(puppy.length);
        return puppy.length;
    }

    public static void printPuppy() {
        System.out.println(Arrays.deepToString(puppy));
    }

    public static void main(String[] args) {
        String[] puppy1 = {"Rex1", "boxer1"};
        Kennel.addPuppy(puppy1);

        Kennel.printPuppy();
        Kennel.getPuppyCount(); // 1

        String[][] puppies2 = {
                {"Buddy", "chihuahua"},
                {"Toby", "chihuahua"},
        };
        Kennel.addSomePuppies(puppies2);

        Kennel.printPuppy();
        Kennel.getPuppyCount(); // 3

        Kennel.isContainPuppy("Buddy"); // true

        String[][] puppies = Kennel.getAllPuppies();
        System.out.println(Arrays.deepToString(puppies));
        // => [[Rex, boxer], [Buddy, chihuahua], [Toby, chihuahua]]

        String[] names = Kennel.getNamesByBreed("chihuahua");
        System.out.println(Arrays.toString(names)); // => [Buddy, Toby]

        Kennel.resetKennel();
        Kennel.getPuppyCount(); // 0
    }
}

// END




