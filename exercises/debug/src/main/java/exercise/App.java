package exercise;

public class App {
    // BEGIN
    public static String getTypeOfTriangle(int a, int b, int c) {
        var typeOfTriangle = "";

        if (a + b <= c || a + c <= b || b + c <= a) {
            typeOfTriangle = "Треугольник не существует";
        } else {
            if ((a == b) && (b == c)) {
                typeOfTriangle = "Равносторонний";
            } else if ((a == b) || (a == c) || (b == c)) {
                typeOfTriangle = "Равнобедренный";
            } else {
                typeOfTriangle = "Разносторонний";
            }
        }
        return typeOfTriangle;
    }

    public static void print(String cmd) {
        System.out.println(cmd);
    }

    public static void main(String[] args) {
        print(getTypeOfTriangle(1, 2, 7)); // "Треугольник не существует"
        print(getTypeOfTriangle(5, 6, 7)); // "Разносторонний"
        print(getTypeOfTriangle(5, 6, 5)); // "Равнобедренный"
        print(getTypeOfTriangle(5, 5, 5)); // "Равносторонний"
    }

    // END
}
