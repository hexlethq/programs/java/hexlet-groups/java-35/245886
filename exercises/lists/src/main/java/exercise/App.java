package exercise;

import java.util.*;

// BEGIN
public class App {
	public static void main(String[] args) {
		App.scrabble("rkqodlw", "world"); // true
		App.scrabble("ajv", "java"); // false
		App.scrabble("avjafff", "JaVa"); // true
		App.scrabble("", "hexlet"); // false
	}

	public static boolean scrabble(String randomOfChar, String world) {
		List<String> listOfWorld = new ArrayList<>(
				Arrays.stream(world
						.toLowerCase()
						.split("")
				).toList()
		);
		List<String> listOfChar = new ArrayList<>(
				Arrays.stream(randomOfChar
						.toLowerCase()
						.split("")
				).toList()
		);

		for (String el : listOfWorld) {
			if (listOfChar.contains(el)) {
				listOfChar.remove(el);
			} else {
				return false;
			}
		}
		return true;
	}
}
//END
