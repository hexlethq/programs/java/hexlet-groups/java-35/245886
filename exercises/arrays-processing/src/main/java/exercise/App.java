package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] arrIntNumbers) {
        int indexOfMaxNegative = -1;
        int negativeElement = -Integer.MAX_VALUE; // Максимальное допустимое отрицательное число в Int без переполнения

//        var r = Arrays.stream(arrIntNumbers).filter(num -> num < indexOfMaxNegative ).toArray();
//        System.out.println(Arrays.toString(r));
// Хотел решить через лямбду, с фильтрацией. Но пока не понял, как потом получить индекс отфильтрованного элемента.

        for (int i = 0; i < arrIntNumbers.length; i++) {
            if (arrIntNumbers[i] < 0) {
                if (arrIntNumbers[i] > negativeElement) {
                    negativeElement = arrIntNumbers[i];
                    indexOfMaxNegative = i;
                }
            }
        }

        return indexOfMaxNegative;
    }

    public static int[] getElementsLessAverage(int[] arrInt) {

        if (arrInt.length != 0) {
            int[] copyArrInt = new int[arrInt.length];
            int inc = 0;
            int sumElementsOfArray = 0;

            // Сумма всех элементов массива
            for (int element : arrInt) {
                sumElementsOfArray += element;
            }

            int averageValue = sumElementsOfArray / arrInt.length; // Среднее значение массива

            for (int i = 0; i < arrInt.length; i++) {
                if (arrInt[i] <= averageValue) {
                    copyArrInt[inc++] = arrInt[i];
                }
            }

            /*
            * Можно пойти по пути постоянного копирования, перезаписывания предыдущей верссии массива в новую
            * Но при большом количестве элементов предполагаю, что операция будет дорогостоющей
            * Проще создать длинну массива равную основе и после вычести последние нулевые элементы
            * */

            int[] trimArray = Arrays.copyOf(copyArrInt, inc);
            System.out.println("trim -> " + Arrays.toString(trimArray));

            return trimArray;
        } else return arrInt;
    }

    public static void main(String[] args) {
//        int[] numbers = {-30, 42, -5, 31, -37, 25, -50};
        int[] numbers = {};
        getIndexOfMaxNegative(numbers);

        int[] numbers1 = {0, 1, 2, 3, 4, 5, 10, 12};
        getElementsLessAverage(numbers1);
    }
    // END
}
